package fr.edessamc.api;

import fr._3dx900.edessamc.EdessaMC;
import fr._3dx900.edessamc.server.EServer;
import fr._3dx900.edessamc.server.ServerState;
import fr.edessamc.api.data.Data;
import fr.edessamc.api.manager.CommandManager;
import fr.edessamc.api.manager.EventManager;
import fr.edessamc.api.manager.MenuManager;
import fr.edessamc.api.manager.MySQLManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.UUID;

public class API extends JavaPlugin {

    private static API instance;

    private static EServer eServer;

    private static EventManager eventManager;
    private static CommandManager commandManager;
    private static MySQLManager mySQLManager;
    private static MenuManager menuManager;

    @Override
    public void onLoad() {
        super.onLoad();

        API.instance = this;

        API.eServer = EdessaMC.getServer(UUID.fromString((String) Data.SERVER_UUID.get(this.getName())));
        API.eServer.setName(this.getName())
                .setPort(this.getServer().getPort())
                .setState(ServerState.LOADING);
    }

    @Override
    public void onEnable() {
        super.onEnable();

        API.eServer.setState(ServerState.ACCESSIBLE);

        API.eventManager = new EventManager(Bukkit.getPluginManager());
        API.commandManager = new CommandManager();
        API.mySQLManager = new MySQLManager();
        API.menuManager = new MenuManager();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static API getInstance() {
        return API.instance;
    }

    public static EventManager getEventManager() {
        return eventManager;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }

    public static MySQLManager getMySQLManager() {
        return mySQLManager;
    }

    public static MenuManager getMenuManager() {
        return menuManager;
    }

    public static EServer getEServer() {
        return eServer;
    }
}
