package fr.edessamc.api.commands;

import fr.edessamc.api.API;
import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.PlayerImpl;
import fr.edessamc.api.player.rank.Rank;
import fr.edessamc.api.utils.text.Message;
import fr.edessamc.api.utils.text.Prefix;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

public class Command implements Runnable {

    private String prefix = "/";

    private String command;
    private String description;
    private String[] usages;
    private Rank rank;

    private EPlayer author;
    private String message;
    private CommandImpl commandInstance;
    private Method method;

    public Command(CommandHandler handler,CommandImpl commandInstance, Method method) {
        this.command = handler.command();
        this.description = handler.description();
        this.usages = handler.usages();
        this.rank = handler.rank();

        this.commandInstance = commandInstance;
        this.method = method;

        API.getCommandManager().addCommand(this.getPrefix() + this.getCommand(), this);
    }

    public String getPrefix() {
        return prefix;
    }

    public String getCommand() {
        return command;
    }

    public String getDescription() {
        return description;
    }

    public String[] getUsages() {
        return usages;
    }

    public String getUsage(Integer index) {
        return usages[index];
    }

    public Rank getRank() {
        return rank;
    }

    public EPlayer getAuthor() {
        return author;
    }

    public Command setAuthor(EPlayer author) {
        this.author = author;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Command setMessage(String message) {
        this.message = message;
        return this;
    }

    public CommandImpl getCommandInstance() {
        return commandInstance;
    }

    public Method getMethod() {
        return method;
    }

    @Override
    public void run() {
        if(!this.getAuthor().getRank().haveAccess(this.getRank())) {
            this.getAuthor().getBukkitPlayer().sendMessage(Prefix.EDESSAMC + Message.NO_PERMISSION + "§r §c(POW. §c§l>=§r §c§l12§c)");
            return;
        }
        Parameter[] parameters = this.getMethod().getParameters();
        Object[] objects = new Object[parameters.length];
        for(int i = 0; i < objects.length; i++) {
            if(parameters[i].getType() == Command.class) objects[i] = this;
            if(parameters[i].getType() == String.class) objects[i] = this.getCommand();
            if(parameters[i].getType() == String[].class) objects[i] = this.getMessage().replace(this.getPrefix() + this.getCommand() + " ", "").split(" ");
            if(parameters[i].getType() == EPlayer.class) objects[i] = this.getAuthor();
        }
        try {
            this.getMethod().invoke(this.getCommandInstance(), objects);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
