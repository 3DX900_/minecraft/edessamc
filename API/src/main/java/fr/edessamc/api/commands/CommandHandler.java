package fr.edessamc.api.commands;

import fr.edessamc.api.player.rank.Rank;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CommandHandler {

    String command();
    String description() default "Aucune description fournie.";
    Rank rank() default Rank.PLAYER;
    String[] usages() default {};
}
