package fr.edessamc.api.data;

import fr.edessamc.api.API;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public enum Data {

    SERVER_UUID("server", "name", "uniqueId"),

    RANK("eplayer", "uniqueId", "rank_name"),

    UNKNOWN(null, null, null);

    private String table;
    private String referenceColumn;
    private String column;

    Data(String table, String referenceColumn, String column) {
        this.table = table;
        this.referenceColumn = referenceColumn;
        this.column = column;
    }

    public String getTable() {
        return table;
    }

    public String getReferenceColumn() {
        return referenceColumn;
    }

    public String getColumn() {
        return column;
    }

    public Object get(String value) {
        try {
            PreparedStatement statement = API.getMySQLManager().getConnection().prepareStatement("SELECT * FROM `" + this.getTable() + "` WHERE `" + this.getReferenceColumn() + "` = '" + value + "'");
            ResultSet result = statement.executeQuery();
            if(!result.next()) {
                this.create(value);
                return this.get(value);
            }
            return result.getObject(this.getColumn());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void create(String value) {
        try {
            API.getMySQLManager().getConnection().prepareStatement("INSERT INTO `" + this.getTable() + "`(`" + this.getReferenceColumn() + "`) VALUES ('" + value + "')").executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
