package fr.edessamc.api.events.player;

import fr.edessamc.api.API;
import fr.edessamc.api.player.EPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

    @EventHandler()
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if(API.getCommandManager().isCommand(event.getMessage().split(" ")[0]))
            new Thread(API.getCommandManager().getCommand(event.getMessage().split(" ")[0]).setAuthor(EPlayer.getPlayer(event.getPlayer())).setMessage(event.getMessage())).start();
    }
}
