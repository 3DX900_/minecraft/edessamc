package fr.edessamc.api.game;

import fr.edessamc.api.game.player.GamePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public abstract class GamePlugin extends JavaPlugin {

    private Game game;

    private List<GamePlayer> players;

    public GamePlugin(Game game) {
        this.game = game;
        this.players = new ArrayList<>();
    }

    public Game getGame() {
        return game;
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public void addPlayer(GamePlayer player) {
        this.players.add(player);
    }

    public void removePlayer(GamePlayer player) {
        this.players.remove(player);
    }
}
