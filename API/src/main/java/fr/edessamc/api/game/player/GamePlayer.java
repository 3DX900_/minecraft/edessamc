package fr.edessamc.api.game.player;

import fr.edessamc.api.game.GamePlugin;
import fr.edessamc.api.player.EPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GamePlayer extends EPlayer {

    private GamePlugin game;

    private List<GamePlayer> spectators;

    public GamePlayer(GamePlugin game, UUID uniqueId) {
        super(uniqueId);
        this.game = game;

        this.spectators = new ArrayList<>();
    }

    public GamePlugin getGame() {
        return game;
    }

    public List<GamePlayer> getSpectators() {
        return spectators;
    }

    public void addSpectator(GamePlayer player) {
        this.spectators.add(player);
    }

    public void removeSpectator(GamePlayer player) {
        this.spectators.remove(player);
    }
}
