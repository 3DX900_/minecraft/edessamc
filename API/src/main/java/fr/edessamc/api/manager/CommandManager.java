package fr.edessamc.api.manager;

import fr.edessamc.api.commands.Command;
import fr.edessamc.api.commands.CommandHandler;
import fr.edessamc.api.commands.CommandImpl;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class CommandManager {

    private Map<String, Command> commands = new HashMap<>();

    public void register(final CommandImpl commandInstance) {
        for(Method method : commandInstance.getClass().getDeclaredMethods()) {
            if(!method.isAnnotationPresent(CommandHandler.class)) continue;
            method.setAccessible(true);
            new Command(method.getAnnotation(CommandHandler.class), commandInstance, method);
        }
    }

    public Map<String, Command> getCommands() {
        return commands;
    }

    public void addCommand(String command, Command commandInstance) {
        this.commands.put(command, commandInstance);
    }

    public Command getCommand(String command) {
        return this.commands.getOrDefault(command, null);
    }

    public Boolean isCommand(String command) {
        return this.commands.containsKey(command);
    }
}
