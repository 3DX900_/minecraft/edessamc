package fr.edessamc.api.manager;

import fr.edessamc.api.API;
import fr.edessamc.api.events.EventListener;
import fr.edessamc.api.events.ListenerType;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

import java.util.ArrayList;
import java.util.List;

public class EventManager {

    private PluginManager manager;

    private List<ListenerType> disabled;

    public EventManager(PluginManager manager) {
        this.manager = manager;
        this.manager.registerEvents(new EventListener(), API.getInstance());
        this.disabled = new ArrayList<>();
    }

    public void register(Listener listener) {
        this.getManager().registerEvents(listener, API.getInstance());
    }

    public void disable(Event event) {}

    public PluginManager getManager() {
        return manager;
    }
}
