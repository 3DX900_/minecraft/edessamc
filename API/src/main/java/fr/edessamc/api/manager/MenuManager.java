package fr.edessamc.api.manager;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.misc.menu.Menu;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MenuManager extends TimerTask {

    private static List<Menu> openedMenus = new ArrayList<>();

    public MenuManager() {
        new Timer().schedule(this, 0, TimeUnit.SECONDS.toMillis(1));
    }

    @Override
    public void run() {
        MenuManager.getOpenedMenus().forEach(Menu::update);
    }

    public static List<Menu> getOpenedMenus() {
        return openedMenus;
    }

    public static Menu getMenu(EPlayer author) {
        for(Menu menu : MenuManager.getOpenedMenus()) {
            if(menu.getAuthor() == author)
                return menu;
        }
        return null;
    }

    public static void addMenu(Menu menu) {
        MenuManager.openedMenus.add(menu);
    }

    public static void removeMenu(Menu menu) {
        MenuManager.openedMenus.remove(menu);
    }
}
