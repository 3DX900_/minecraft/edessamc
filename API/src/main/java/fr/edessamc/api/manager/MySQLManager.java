package fr.edessamc.api.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLManager {

    protected final String DATABASE_DRIVER = "com.mysql.jdbc.Driver";
    protected final String DATABASE_URL = "jdbc:mysql://localhost:3306/edessamc";
    protected final String USERNAME = "edessamc_api";
    protected final String PASSWORD = "YLFPmp#mtKu#4oxWFgJA5@MHTEW&FZILLvDUeIBcspX%waItKVeVKN5T@fmXZqTD7i5*N^8HpJb7zDzDpInE2g6&vKu72IaG2oJLsojIHdC%Ewnu&Yc&5hiAYvgVYUtu";
    protected final String MAX_POOL = "250";

    private Connection connection;
    private Properties properties;

    public MySQLManager() {
        this.connect();
    }

    public Connection getConnection() {
        return this.connect();
    }

    protected Properties getProperties() {
        if(this.properties == null) {
            this.properties = new Properties();
            this.properties.setProperty("user", this.USERNAME);
            this.properties.setProperty("password", this.PASSWORD);
            this.properties.setProperty("MaxPooledStatements", this.MAX_POOL);
        }
        return this.properties;
    }

    private Connection connect() {
        if(this.connection == null) {
            try {
                Class.forName(this.DATABASE_DRIVER);
                this.connection = DriverManager.getConnection(this.DATABASE_URL, getProperties());
            } catch (ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }
        return this.connection;
    }

    public void disconnect() {
        if(this.connection != null) {
            try {
                this.connection.close();
                this.connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
