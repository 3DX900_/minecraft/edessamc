package fr.edessamc.api.player;

import fr.edessamc.api.manager.MenuManager;
import fr.edessamc.api.player.misc.Booster;
import fr.edessamc.api.player.misc.menu.Menu;
import fr.edessamc.api.player.rank.Rank;
import fr.edessamc.api.utils.builder.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class EPlayer extends PlayerImpl {

    private static List<EPlayer> players = new ArrayList<>();

    private Rank rank;
    private Integer experience;

    private List<EPlayer> friends;
    private List<EPlayer> friendRequests;
    private Group group;

    private List<Booster> boosters;
    private Integer pieces;

    private List<Menu> menus;
    private EPlayer lastPrivateMessage;

    public EPlayer(UUID uniqueId) {
        super(uniqueId);
        this.rank = Rank.PLAYER;
        this.experience = 0;

        this.friends = new ArrayList<>();
        this.friendRequests = new ArrayList<>();
        this.group = new Group(this);

        this.boosters = new ArrayList<>();
        this.pieces = 0;

        this.menus = new ArrayList<>();
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer level) {
        this.experience = level;
    }

    public void addExperience(Integer level) {
        this.experience += level;
    }

    public List<EPlayer> getFriends() {
        return this.friends;
    }

    public List<EPlayer> getOnlineFriends() {
        List<EPlayer> friends = new ArrayList<>();
        this.friends.forEach(friend -> {
            if(friend.getBukkitPlayer() != null || friend.getBukkitPlayer().isOnline())
                friends.add(friend);
        });
        return friends;
    }

    public Boolean areFriend(EPlayer friend) {
        return this.friends.contains(friend);
    }

    public void addFriend(EPlayer... players) {
        if(this.getFriends().size() >= this.getRank().getMaxFriends()) return;
        if(!this.getFriendRequests().containsAll(List.of(players))) return;
        this.friends.addAll(List.of(players));
    }

    public void removeFriend(EPlayer... players) {
        this.friends.removeAll(List.of(players));
    }

    public List<EPlayer> getFriendRequests() {
        return this.friendRequests;
    }

    public Boolean containsFriendRequest(EPlayer requester) {
        return this.friendRequests.contains(requester);
    }

    public void addFriendRequest(EPlayer requester) {
        this.friendRequests.add(requester);
    }

    public void removeFriendRequest(EPlayer reqester) {
        this.friendRequests.remove(reqester);
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void addToGroup(EPlayer... players) {
        if(this.group.getMembers().size() >= this.group.getOwner().getRank().getMaxGroup()) return;
        this.group.addMember(players);
    }

    public void removeFromGroup(EPlayer... players) {
        if(this != this.group.getOwner()) return;
        this.group.removeMember(players);
    }

    public List<Booster> getBoosters() {
        return boosters;
    }

    public void addBooster(Date endDate, Integer multiplicator) {
        this.boosters.add(new Booster(new Date(), endDate, multiplicator));
    }

    public List<Booster> getActiveBoosters() {
        List<Booster> activeBoosters = new ArrayList<>();
        for(Booster booster : this.boosters) {
            if(booster.getEndDate().getTime() < new Date().getTime())
                activeBoosters.add(booster);
        }
        return activeBoosters;
    }

    public Integer getBoosterMultiplicator() {
        Integer boosterMultiplicator = 0;
        for(Booster booster : this.getActiveBoosters()) {
            boosterMultiplicator += booster.getMultiplicator();
        }
        return boosterMultiplicator;
    }

    public Integer getPieces() {
        return pieces;
    }

    public void addPieces(Integer pieces) {
        this.pieces += pieces;
    }

    public void removePieces(Integer pieces) {
        this.pieces -= pieces;
    }

    public Boolean isActualMenu() {
        return MenuManager.getMenu(this) != null;
    }

    public Menu getActualMenu() {
        return MenuManager.getMenu(this);
    }

    public Menu getLastMenu() {
        return this.menus.get(this.menus.size() - 1);
    }

    public Menu openMenu(Menu menu) {
        if(this.isActualMenu()) menu.setPrevious(this.getActualMenu());
        this.getBukkitPlayer().openInventory(menu.getInventory());
        this.menus.add(menu);
        return menu;
    }

    public void closeMenu() {
        this.getBukkitPlayer().closeInventory();
    }

    public EPlayer getLastPrivateMessage() {
        return lastPrivateMessage;
    }

    public Boolean isLastPrivateMessage() {
        return this.lastPrivateMessage != null;
    }

    public void setLastPrivateMessage(EPlayer lastPrivateMessage) {
        this.lastPrivateMessage = lastPrivateMessage;
    }

    public void teleport(Location location) {
        this.getBukkitPlayer().teleport(location);
    }

    public void fillInventory() {
        this.fillInventory(Material.AIR);
    }

    public void fillInventory(Material type) {
        this.fillInventory(new ItemBuilder(type));
    }

    public void fillInventory(ItemBuilder item) {
        this.fillInventory(item.build());
    }

    public void fillInventory(ItemStack item) {
        for(int i = 0; i < 44; i++) {
            this.getBukkitPlayer().getInventory().setItem(i, item);
        }
    }

    public void setInventoryItem(Integer index) {
        this.setInventoryItem(index, Material.AIR);
    }

    public void setInventoryItem(Integer index, Material type) {
        this.setInventoryItem(index, new ItemBuilder(type));
    }

    public void setInventoryItem(Integer index, ItemBuilder item) {
        this.setInventoryItem(index, item.build());
    }

    public void setInventoryItem(Integer index, ItemStack item) {
        this.getBukkitPlayer().getInventory().setItem(index, item);
    }

    public static List<EPlayer> getPlayers() {
        return players;
    }

    public static EPlayer getPlayer(String playerName) {
        return EPlayer.getPlayer(Bukkit.getPlayer(playerName));
    }

    public static EPlayer getPlayer(Player player) {
        return EPlayer.getPlayer(player.getUniqueId());
    }

    public static EPlayer getPlayer(UUID uniqueId) {
        for(EPlayer player : EPlayer.getPlayers()) {
            if(player.getUniqueId() == uniqueId)
                return player;
        }
        return new EPlayer(uniqueId);
    }
}
