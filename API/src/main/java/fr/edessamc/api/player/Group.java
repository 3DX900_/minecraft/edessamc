package fr.edessamc.api.player;

import java.util.List;

public class Group {

    private EPlayer owner;
    private List<EPlayer> members;

    public Group(EPlayer owner, EPlayer... members) {
        this(owner, List.of(members));
    }

    public Group(EPlayer owner, List<EPlayer> members) {
        this.owner = owner;
        this.members = members;
    }

    public EPlayer getOwner() {
        return owner;
    }

    public void setOwner(EPlayer player) {
        if(!members.contains(player)) return;
        this.owner = player;
    }

    public List<EPlayer> getMembers() {
        return members;
    }

    public void addMember(EPlayer... players) {
        this.members.addAll(List.of(players));
    }

    public void removeMember(EPlayer... players) {
        this.members.removeAll(List.of(players));
    }
}
