package fr.edessamc.api.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public abstract class PlayerImpl {

    private UUID uniqueId;
    private Player bukkitPlayer;

    public PlayerImpl(UUID uniqueId) {
        this.uniqueId = uniqueId;
        this.bukkitPlayer = Bukkit.getPlayer(uniqueId);
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public Player getBukkitPlayer() {
        return bukkitPlayer;
    }
}
