package fr.edessamc.api.player.misc;

import java.util.Date;

public class Booster {

    private Date startDate;
    private Date endDate;

    private Integer multiplicator;

    public Booster(Date startDate, Date endDate, Integer multiplicator) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.multiplicator = multiplicator;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Integer getMultiplicator() {
        return multiplicator;
    }

    public void setMultiplicator(Integer multiplicator) {
        this.multiplicator = multiplicator;
    }
}
