package fr.edessamc.api.player.misc;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Consumer;

public class ClickableItem {

    private ItemStack item;
    private Consumer<InventoryClickEvent> action;

    public ClickableItem(ItemStack item, Consumer<InventoryClickEvent> action) {
        this.item = item;
        this.action = action;
    }

    public ItemStack getItem() {
        return item;
    }

    public Consumer<InventoryClickEvent> getAction() {
        return action;
    }

    public static ClickableItem of(ItemStack item, Consumer<InventoryClickEvent> action) {
        return new ClickableItem(item, action);
    }

    public void run(InventoryClickEvent event) {
        action.accept(event);
    }
}
