package fr.edessamc.api.player.misc;

public enum Level {

    LVL_1("1", 100),
    LVL_2("2", 200),
    MAX("MAX", 10000);

    private String level;
    private Integer experience;

    Level(String level, Integer experience) {
        this.level = level;
        this.experience = experience;
    }

    public String getLevel() {
        return level;
    }

    public Integer getExperience() {
        return experience;
    }

    public static Level get(Integer experience) {
        for(Level level : Level.values()) {
            if((experience - level.getExperience()) > 0) {
                experience -= level.getExperience();
                continue;
            }
            return level;
        }
        return Level.LVL_1;
    }

    public String toString() {
        return this.getLevel();
    }
}
