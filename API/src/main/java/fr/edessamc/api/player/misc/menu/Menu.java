package fr.edessamc.api.player.misc.menu;

import fr.edessamc.api.manager.MenuManager;
import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.misc.ClickableItem;
import fr.edessamc.api.utils.builder.InventoryBuilder;
import fr.edessamc.api.utils.builder.ItemBuilder;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Consumer;

public abstract class Menu extends InventoryBuilder implements Listener {

    private EPlayer author;

    private ItemBuilder item;

    private Menu previous;

    public Menu(String title, ItemBuilder item, EPlayer author) {
        super(title);
        this.item = item;
        this.author = author;

        MenuManager.addMenu(this);
    }

    public EPlayer getAuthor() {
        return author;
    }

    public ItemBuilder getItem() {
        return item;
    }

    public Menu getPrevious() {
        return previous;
    }

    public void setPrevious(Menu previous) {
        this.previous = previous;
    }

    public void close() {
        this.getAuthor().closeMenu();
        MenuManager.removeMenu(this);
    }

    public ClickableItem setItem(Integer index, ItemStack item, Consumer<InventoryClickEvent> action) {
        this.getInventory().setItem(index, item);
        return ClickableItem.of(item, action);
    }

    public abstract void init();
    public abstract void update();
}
