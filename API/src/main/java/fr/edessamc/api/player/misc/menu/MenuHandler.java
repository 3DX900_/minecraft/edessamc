package fr.edessamc.api.player.misc.menu;

public @interface MenuHandler {

    String name();

    int rows() default 6;
}
