package fr.edessamc.api.player.rank;

import org.bukkit.ChatColor;

public enum Rank {

    ADMINISTRATOR("ADMINISTRATOR", "§cAdministrateur", "§cAdmin", 11),
    RESPONSIBLE("RESPONSIBLE", "§9Gérant", 10),
    MODERATOR("MODERATOR", "§9Modérateur", "§9Mod", 9),
    HELPER("HELPER", "§3Guide", 8),
    STAFF("STAFF", "§2Staff", 7),
    FRIEND("FRIEND", "§aAmi", 6),
    PARTNER("PARTNER", "§2Partenaire", 6),
    VIDEOGRAPHER("VIDEOGRAPHER", "§dVidéaste", 6),
    COMET("COMET", "§bComète ☄", 5),
    STAR("STAR", "§6Étoile ☀", 4),
    VIPPlus("VIP+", "§eVIP+", 3, 20, 8),
    VIP("VIP", "§fVIP", 2, 10, 6),
    MINIVIP("MINIVIP", "§8MiniVIP", 1, 7, 5),
    PLAYER("PLAYER", "§c❤§7", 0, ChatColor.GRAY, 5, 3);

    private String name;
    private String prefix;
    private String quickPrefix;

    private Integer power;

    private ChatColor color;

    private Integer maxFriends;
    private Integer maxGroup;

    Rank(String name, String prefix, Integer power) {
        this(name, prefix, prefix, power, ChatColor.WHITE, 50, 10);
    }

    Rank(String name, String prefix, Integer power, Integer maxFriends, Integer maxGroup) {
        this(name, prefix, power, ChatColor.WHITE, maxFriends, maxGroup);
    }

    Rank(String name, String prefix, String quickPrefix, Integer power) {
        this(name, prefix, quickPrefix, power, ChatColor.WHITE, 50, 10);
    }

    Rank(String name, String prefix, Integer power, ChatColor color, Integer maxFriends, Integer maxGroup) {
        this(name, prefix, prefix, power, color, maxFriends, maxGroup);
    }

    Rank(String name, String prefix, String quickPrefix, Integer power, ChatColor color, Integer maxFriends, Integer maxGroup) {
        this.name = name;
        this.prefix = prefix;
        this.quickPrefix = quickPrefix;
        this.power = power;
        this.color = color;
        this.maxFriends = maxFriends;
        this.maxGroup = maxGroup;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getQuickPrefix() {
        return quickPrefix;
    }

    public Integer getPower() {
        return power;
    }

    public ChatColor getColor() {
        return color;
    }

    public Integer getMaxFriends() {
        return maxFriends;
    }

    public Integer getMaxGroup() {
        return maxGroup;
    }

    public Boolean haveAccess(Rank rank) {
        return this.getPower() > rank.getPower();
    }

    public static Rank getByName(String name) {
        for(Rank rank : Rank.values()) {
            if(rank.getName().equalsIgnoreCase(name))
                return rank;
        }
        return Rank.PLAYER;
    }
}
