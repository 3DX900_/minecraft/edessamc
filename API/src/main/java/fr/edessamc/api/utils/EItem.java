package fr.edessamc.api.utils;

import fr.edessamc.api.utils.builder.ItemBuilder;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class EItem {

    public static final ItemBuilder MAIN_MENU = new ItemBuilder(Material.COMPASS, "§aMenu Principal §7(Clic droit)", true);

    public static final ItemStack MAIN_HUB = new ItemBuilder(Material.PLAYER_HEAD, "§aHub Principal").addLores("§7Retournez au centre du Hub.", "", "§a» Cliquez pour être téléporté.").build();
    public static final ItemStack HUB_SELECTOR = new ItemBuilder(Material.PLAYER_HEAD, "§aSélecteur de Hub").addLores("§7Téléportez-vous vers un Hub.", "", "§a» Cliquez pour être téléporté.").build();
    public static final ItemStack DOMINATION = new ItemBuilder(Material.IRON_SWORD, "§aDomination").addLores("§fCombat § Stratégie", "", "§7§7L'objectif est basé sur la capture", "§7d'une zone. Avec l'aide de votre équipe,", "§7tuez vos adversaires et occupez leur", "§7zone de capture pour terminer et", "§7remporter la partie.", "", "§f➟ Ce jeu comporte un bonus !||§7Multiplicateur de gains §f2.0x||||§a» Cliquez pour vous y connecter.||§f0 §7joueur en jeu !").build();

    public static final ItemStack UNKNOWN = null;
}
