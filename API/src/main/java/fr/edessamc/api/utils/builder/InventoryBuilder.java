package fr.edessamc.api.utils.builder;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

public class InventoryBuilder {

    private InventoryHolder owner;
    private InventoryType type;
    private String title;

    private Inventory inventory;

    public InventoryBuilder() {
        this(null, InventoryType.CHEST, "Chest");
    }

    public InventoryBuilder(InventoryType type) {
        this(null, type, "Chest");
    }

    public InventoryBuilder(String title) {
        this(null, InventoryType.CHEST, title);
    }

    public InventoryBuilder(InventoryType type, String title) {
        this(null, type, title);
    }

    public InventoryBuilder(InventoryHolder owner, InventoryType type, String title) {
        this.owner = owner;
        this.type = type;
        this.title = title;
    }

    public InventoryHolder getOwner() {
        return owner;
    }

    public InventoryType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public Inventory build() {
        this.inventory = Bukkit.createInventory(this.getOwner(), this.getType(), this.getTitle());
        return this.inventory;
    }
}
