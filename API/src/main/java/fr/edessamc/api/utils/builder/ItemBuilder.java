package fr.edessamc.api.utils.builder;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.*;

public class ItemBuilder {

    private Material type;

    private String name;
    private Collection<String> lores;
    private ItemFlag[] flags;
    private Boolean unbreakable;

    private Map<Enchantment, Integer> enchantments;
    private Integer amount;
    private Short damage;
    private Byte data;

    public ItemBuilder(Material type) {
        this(type, "", new ArrayList<>(), new ItemFlag[]{}, false, new HashMap<>(), 1, (short) 0, (byte) 0);
    }

    public ItemBuilder(Material type, String name) {
        this(type, name, new ArrayList<>(), new ItemFlag[]{}, false, new HashMap<>(), 1, (short) 0, (byte) 0);
    }

    public ItemBuilder(Material type, String name, Boolean unbreakable) {
        this(type, name, new ArrayList<>(), new ItemFlag[]{}, unbreakable, new HashMap<>(), 1, (short) 0, (byte) 0);
    }

    public ItemBuilder(Material type, String name, Collection<String> lores, Boolean unbreakable) {
        this(type, name, lores, new ItemFlag[]{}, unbreakable, new HashMap<>(), 1, (short) 0, (byte) 0);
    }

    public ItemBuilder(Material type, String name, Collection<String> lores, ItemFlag[] flags, Boolean unbreakable, Integer amount,
                       Short damage, Byte data) {}

    public ItemBuilder(Material type, String name, Collection<String> lores, ItemFlag[] flags, Boolean unbreakable, Map<Enchantment, Integer> enchantments,
                       Short damage, Byte data) {}

    public ItemBuilder(Material type, String name, Collection<String> lores, ItemFlag[] flags, Boolean unbreakable, Map<Enchantment, Integer> enchantments, Integer amount,
                       Byte data) {}

    public ItemBuilder(Material type, String name, Collection<String> lores, ItemFlag[] flags, Boolean unbreakable, Map<Enchantment, Integer> enchantments, Integer amount,
                       Short damage) {}

    public ItemBuilder(Material type, String name, Collection<String> lores, ItemFlag[] flags, Boolean unbreakable, Map<Enchantment, Integer> enchantments, Integer amount,
                       Short damage, Byte data) {
        this.type = type;
        this.name = name;
        this.lores = lores;
        this.flags = flags;
        this.unbreakable = unbreakable;
        this.enchantments = enchantments;
        this.amount = amount;
        this.damage = damage;
        this.data = data;
    }

    public Material getType() {
        return type;
    }

    public ItemBuilder setType(Material type) {
        this.type = type;
        return this;
    }

    public String getName() {
        return this.name;
    }

    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public Collection<String> getLores() {
        return this.lores;
    }

    public ItemBuilder setLore(Collection<String> lore) {
        this.lores = lore;
        return this;
    }

    public ItemBuilder addLores(String... lores) {
        this.lores.addAll(Arrays.asList(lores));
        return this;
    }

    public ItemBuilder removeLores(String... lores) {
        this.lores.removeAll(Arrays.asList(lores));
        return this;
    }

    public ItemFlag[] getFlags() {
        return this.flags;
    }

    public ItemBuilder setFlags(ItemFlag[] flags) {
        this.flags = flags;
        return this;
    }

    public ItemBuilder addFlag(ItemFlag... flags) {
        for(ItemFlag flag : flags) {
            this.flags[this.flags.length] = flag;
        }
        return this;
    }

    public Boolean isUnbreakable() {
        return this.unbreakable;
    }

    public ItemBuilder setUnbreakable(Boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public Map<Enchantment, Integer> getEnchantments() {
        return this.enchantments;
    }

    public ItemBuilder setEnchantments(Map<Enchantment, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, Integer level) {
        this.enchantments.put(enchantment, level);
        return this;
    }

    public ItemBuilder removeEnchantment(Enchantment enchantment) {
        this.enchantments.remove(enchantment);
        return this;
    }

    public Integer getAmount() {
        return this.amount;
    }

    public ItemBuilder setAmount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public Short getDamage() {
        return this.damage;
    }

    public ItemBuilder setDamage(Short damage) {
        this.damage = damage;
        return this;
    }

    public Byte getData() {
        return this.data;
    }

    public ItemBuilder setData(Byte data) {
        this.data = data;
        return this;
    }

    @Deprecated
    public ItemStack build() {
        ItemStack item = new ItemStack(this.getType());
        if(item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(this.getName());
            meta.setLore((List<String>) this.getLores());
            meta.addItemFlags(this.getFlags());
            meta.setUnbreakable(this.isUnbreakable());

            item.setItemMeta(meta);
        }
        item.addUnsafeEnchantments(this.getEnchantments());
        item.setAmount(this.getAmount());
        item.setDurability(this.getDamage());
        item.setData(new MaterialData(this.getType(), this.getData()));
        return item;
    }
}
