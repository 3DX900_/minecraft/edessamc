package fr.edessamc.api.utils.text;

public class Prefix {

    public static final String EDESSAMC = "§6§lEde'Bot §8» ";

    public static final String FRIEND = "§dAmis §8» ";
    public static final String GROUP = "§eGroupe §8» ";
    public static final String RANK = "§bGrades §8» ";
}
