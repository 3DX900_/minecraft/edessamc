package fr.edessamc.bot;

import fr._3dx900.ultrajda.UltraJDA;
import fr.edessamc.bot.commands.PollCommand;
import fr.edessamc.bot.manager.BotManager;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;

import javax.security.auth.login.LoginException;

public final class Bot extends UltraJDA {

    public static final Long UNIQUE_GUILDID = 536654173945724942L;

    public static void main(String[] args) {
        if(args.length < 1)
            throw new IllegalArgumentException("You have to enter the bot private token for start it!");
        try {
            new Bot(new JDABuilder(AccountType.BOT)
                    .setToken(args[0])
                    .build()
            );
        } catch (LoginException e) {
            e.printStackTrace();
        }
    }

    public Bot(JDA api) {
        super(api);
        new BotManager();

        Bot.getCommandManager().setPrefix("!");

        Bot.registerCommand(new PollCommand());
    }
}
