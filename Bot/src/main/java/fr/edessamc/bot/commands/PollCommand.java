package fr.edessamc.bot.commands;

import fr._3dx900.ultrajda.UltraJDA;
import fr._3dx900.ultrajda.commands.Command;
import fr._3dx900.ultrajda.commands.CommandHandler;
import fr.edessamc.bot.Bot;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;

import java.awt.*;

public class PollCommand {

    @CommandHandler(command = "poll", permission = Permission.MANAGE_CHANNEL)
    public void process(Channel[] channels, MessageChannel channel, String arg, Guild guild, Command command, String[] args) {
        if(args.length < 1) {
            channel.sendMessage(Rapid.error()
                    .setDescription("Vous utilisez une mauvaise syntaxe de cette commande...")
                    .setFooter("Syntaxe : '" + UltraJDA.getCommandManager().getDefaultPrefix() + arg + " createLogs'", "https://3DX900.fr/src/img/info.png")
                    .build()
            ).queue();
            return;
        }
        if(args.length < 2 || channels.length != 1) {
            channel.sendMessage(Rapid.error()
                    .setDescription("Vous utilisez une mauvaise syntaxe de cette commande...")
                    .setFooter("Syntaxe : '" + UltraJDA.getCommandManager().getDefaultPrefix() + arg + " #salon <Message du sondage>'", "https://3DX900.fr/src/img/info.png")
                    .build()
            ).queue();
            return;
        }
        if(channels[0].getType() == ChannelType.TEXT) {
            if(!((TextChannel) channels[0]).canTalk(guild.getMember(Bot.getAPI().getSelfUser()))) {
                channel.sendMessage(Rapid.error(command)
                        .setDescription("Je ne peux pas écrire dans le salon " + ((TextChannel) channels[0]).getAsMention() + ".")
                        .addField(Rapid.MET_PROBLEM)
                        .build()
                ).complete();
                return;
            }
            StringBuilder builder = new StringBuilder();
            for(String string : args) {
                if(!string.equalsIgnoreCase("#" + channels[0].getName()))
                    builder.append(string + " ");
            }
            Message message = channel.sendMessage(new EmbedBuilder()
                    .setAuthor(guild.getName() + " » Sondage", null, "https://3DX900.fr/src/img/polls.png")
                    .setDescription("**" + builder.toString() + "**")
                    .setFooter("Veuillez répondre à ce sondage par les réactions ci-dessous.", "https://3DX900.fr/src/img/info.png")
                    .setColor(Color.decode("#3498db"))
                    .build()
            ).complete();
            message.addReaction(Bot.getAPI().getEmoteById(534352342850600960L)).complete();
            message.addReaction(Bot.getAPI().getEmoteById(534352286353457162L)).complete();
        }
    }
}
