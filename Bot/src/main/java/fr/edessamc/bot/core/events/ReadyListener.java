package fr.edessamc.bot.core.events;

import fr.edessamc.bot.Bot;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.ReadyEvent;

public class ReadyListener {

    public ReadyListener(ReadyEvent event) {
        for(Guild guild : Bot.getAPI().getGuilds()) {
            if(guild.getIdLong() != Bot.UNIQUE_GUILDID && guild.getIdLong() != 534350762940170242L) {
                guild.getDefaultChannel().sendMessage(Rapid.error().setDescription("Je suis désolé de vous décevoir, mais je n'ai pas les qualités requises pour être utilisé sur d'autres serveur Discord, au revoir !\n*Bip, bip, fin de transmition.*").build()).complete();
                guild.leave().complete();
                return;
            }
        }
    }
}
