package fr.edessamc.bot.core.events.guild;

import fr.edessamc.bot.Bot;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;

public class GuildJoinListener {

    public GuildJoinListener(GuildJoinEvent event) {
        if(event.getGuild().getIdLong() != Bot.UNIQUE_GUILDID) {
            event.getGuild().getDefaultChannel().sendMessage(Rapid.error().setDescription("Je suis désolé de vous décevoir, mais je n'ai pas les qualités requises pour être utilisé sur d'autres serveur Discord, au revoir !\n*Bip, bip, fin de transmition.*").build()).complete();
            event.getGuild().leave().complete();
            return;
        }
    }
}
