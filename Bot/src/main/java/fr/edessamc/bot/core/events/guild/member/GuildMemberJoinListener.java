package fr.edessamc.bot.core.events.guild.member;

import fr.edessamc.bot.manager.BotManager;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;

import java.awt.*;

public class GuildMemberJoinListener {

    public GuildMemberJoinListener(GuildMemberJoinEvent event) {
        if(event.getUser().isBot()) return;
        BotManager.getTextChannel("logs").sendMessage(Rapid.info().setColor(Color.decode("#2ecc71")).setDescription("**" + event.getMember().getAsMention() + "** vient de rejoindre le serveur. Nous sommes maintenant __" + event.getGuild().getMembers().size() + "__ sur le serveur Discord.").build()).complete();
    }
}
