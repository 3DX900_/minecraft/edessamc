package fr.edessamc.bot.core.events.guild.member;

import fr.edessamc.bot.manager.BotManager;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;

import java.awt.*;

public class GuildMemberLeaveListener {

    public GuildMemberLeaveListener(GuildMemberLeaveEvent event) {
        if(event.getUser().isBot()) return;
        BotManager.getTextChannel("logs").sendMessage(Rapid.info().setColor(Color.decode("#e74c3c")).setDescription("**" + event.getMember().getAsMention() + "** vient de quitter le serveur. Nous sommes maintenant __" + event.getGuild().getMembers().size() + "__ sur le serveur Discord.").build()).complete();
    }
}
