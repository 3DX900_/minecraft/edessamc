package fr.edessamc.bot.core.events.guild.message;

import fr.edessamc.bot.manager.BotManager;
import fr.edessamc.bot.utils.Rapid;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent;

public class GuildMessageReactionRemoveListener {

    public GuildMessageReactionRemoveListener(GuildMessageReactionRemoveEvent event) {
        if(event.getUser().isBot()) return;
        BotManager.getTextChannel("logs").sendMessage(Rapid.info().setDescription(event.getMember().getAsMention() + " a retiré la réaction " + event.getReactionEmote().getEmote() + " du sondage !").build()).complete();
    }
}
