package fr.edessamc.bot.manager;

import fr.edessamc.bot.Bot;
import net.dv8tion.jda.core.entities.Channel;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.TextChannel;

import java.util.List;

public class BotManager {


    public BotManager() {
        new EventManager();
    }

    public static List<Channel> getChannels() {
        return Bot.getAPI().getGuildById(Bot.UNIQUE_GUILDID).getChannels();
    }

    public static TextChannel getTextChannel(String name) {
        return (TextChannel) BotManager.getChannel(ChannelType.TEXT, name);
    }

    public static Channel getChannel(ChannelType type, String name) {
        if(BotManager.getChannel(name).getType() == type)
            return BotManager.getChannel(name);
        return null;
    }

    public static Channel getChannel(String name) {
        for(Channel channel : BotManager.getChannels()) {
            if(channel.getName().equalsIgnoreCase(name))
                return channel;
        }
        return null;
    }
}
