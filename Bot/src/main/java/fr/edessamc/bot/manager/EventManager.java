package fr.edessamc.bot.manager;

import fr.edessamc.bot.Bot;
import fr.edessamc.bot.core.events.ReadyListener;
import fr.edessamc.bot.core.events.guild.GuildJoinListener;
import fr.edessamc.bot.core.events.guild.member.GuildMemberJoinListener;
import fr.edessamc.bot.core.events.guild.member.GuildMemberLeaveListener;
import fr.edessamc.bot.core.events.guild.message.GuildMessageReactionAddListener;
import fr.edessamc.bot.core.events.guild.message.GuildMessageReactionRemoveListener;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.guild.GuildJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionAddEvent;
import net.dv8tion.jda.core.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class EventManager extends ListenerAdapter {

    public EventManager() {
        Bot.getAPI().addEventListener(this);
    }

    @Override
    public void onReady(ReadyEvent event) {
        super.onReady(event);
        new ReadyListener(event);
    }

    @Override
    public void onGuildJoin(GuildJoinEvent event) {
        super.onGuildJoin(event);
        new GuildJoinListener(event);
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        super.onGuildMemberJoin(event);
        new GuildMemberJoinListener(event);
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        super.onGuildMemberLeave(event);
        new GuildMemberLeaveListener(event);
    }

    @Override
    public void onGuildMessageReactionAdd(GuildMessageReactionAddEvent event) {
        super.onGuildMessageReactionAdd(event);
        new GuildMessageReactionAddListener(event);
    }

    @Override
    public void onGuildMessageReactionRemove(GuildMessageReactionRemoveEvent event) {
        super.onGuildMessageReactionRemove(event);
        new GuildMessageReactionRemoveListener(event);
    }
}
