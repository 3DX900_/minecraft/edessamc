package fr.edessamc.bot.utils;

import fr._3dx900.ultrajda.UltraJDA;
import fr._3dx900.ultrajda.commands.Command;
import fr.edessamc.bot.Bot;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.MessageEmbed;

import java.awt.*;

public class Rapid {

    public static final MessageEmbed.Field MET_PROBLEM = new MessageEmbed.Field("\n:small_orange_diamond: Tu rencontres un problème ?", "Si tu souhaites obtenir davantage d'informations, que tu as besoin d'aide ou que tu souhaites me parler, rejoins-moi !\n"
            + "()", false);

    public static final EmbedBuilder success(final Command command) {
        return Rapid.success().setFooter("par " + command.getMessage().getMember().getEffectiveName() + " (" + UltraJDA.getCommandManager().getPrefix(command.getMessage().getGuild().getIdLong()) + command.getCommand() + ")", command.getMessage().getAuthor().getAvatarUrl());
    }

    public static final EmbedBuilder success() {
        return new EmbedBuilder()
                .setAuthor(Bot.getAPI().getSelfUser().getName() + " » Succès !", null, "https://3DX900.fr/src/img/accept.png")
                .setColor(Color.decode("#2ecc71"));
    }

    public static final EmbedBuilder error(final Command command) {
        return Rapid.error().setFooter("par " + command.getMessage().getMember().getEffectiveName() + " (" + UltraJDA.getCommandManager().getPrefix(command.getMessage().getGuild().getIdLong()) + command.getCommand() + ")", command.getMessage().getAuthor().getAvatarUrl());
    }

    public static EmbedBuilder error() {
        return new EmbedBuilder()
                .setAuthor(Bot.getAPI().getSelfUser().getName() + " » Erreur", null, Bot.getAPI().getEmoteById(534361936574283776L).getImageUrl())
                .setColor(Color.decode("#e74c3c"));
    }

    public static EmbedBuilder info() {
        return new EmbedBuilder()
                .setAuthor(Bot.getAPI().getSelfUser().getName() + " » Information", null, Bot.getAPI().getEmoteById(534361936574283776L).getImageUrl())
                .setColor(Color.decode("#f39c12"));
    }
}
