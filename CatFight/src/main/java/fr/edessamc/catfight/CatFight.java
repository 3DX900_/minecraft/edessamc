package fr.edessamc.catfight;

import fr._3dx900.edessamc.server.ServerType;
import fr.edessamc.api.API;
import fr.edessamc.api.game.GamePlugin;

public class CatFight extends GamePlugin {

    @Override
    public void onLoad() {
        super.onLoad();

        API.getEServer().setType(ServerType.GAME);
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
