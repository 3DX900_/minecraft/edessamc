package fr._3dx900.core;

import fr._3dx900.core.commands.FriendCommand;
import fr._3dx900.core.commands.RankCommand;
import fr._3dx900.core.event.player.AsyncPlayerChatListener;
import fr.edessamc.api.API;
import org.bukkit.plugin.java.JavaPlugin;

public class Core extends JavaPlugin {

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void onEnable() {
        super.onEnable();

        API.getEventManager().register(new AsyncPlayerChatListener());

        API.getCommandManager().register(new RankCommand());
        API.getCommandManager().register(new FriendCommand());
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
