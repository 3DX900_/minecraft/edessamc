package fr._3dx900.core.commands;

import fr.edessamc.api.commands.Command;
import fr.edessamc.api.commands.CommandHandler;
import fr.edessamc.api.commands.CommandImpl;
import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.utils.text.Prefix;
import org.bukkit.Bukkit;

public class FriendCommand implements CommandImpl {

    @CommandHandler(command = "friend", description = "", usages = {"help", "list", "add", "remove", "accept", "decline"})
    public void process(String[] args, Command command, EPlayer sender) {
        if(args.length < 1 || args[0].equalsIgnoreCase(command.getUsage(0))) {
            //TODO: HELP
            return;
        }
        if(args[0].equalsIgnoreCase(command.getUsage(1))) {
            //TODO: FRIENDS INV
            return;
        }
        if(args.length < 2 || Bukkit.getPlayer(args[1]) == null) {
            //TODO: HELP
            return;
        }
        EPlayer target = EPlayer.getPlayer(args[1]);
        if(args[0].equalsIgnoreCase(command.getUsage(2))) {
            if(target == sender) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous ne pouvez pas être ami avec ce joueur pour des raisons internes.");
                return;
            }
            if(target.containsFriendRequest(sender)) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous avez déjà envoyé une demande d'ami à ce joueur !");
                return;
            }
            if(sender.areFriend(target)) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cCe joueur est déjà votre ami !");
                return;
            }
            if(sender.containsFriendRequest(target)) {
                sender.getBukkitPlayer().performCommand("/friend accept " + target.getBukkitPlayer().getDisplayName());
                return;
            }
            target.addFriendRequest(sender);
            sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§7Demande d'ami envoyée à §b" + target.getBukkitPlayer().getDisplayName() + "§7.");
            if(target.getBukkitPlayer().isOnline()) target.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + sender.getBukkitPlayer().getDisplayName() + "§r §7souhaite devenir votre ami !");
            //TODO: TELLRAW FOR ACCEPT OR DECLINE
            return;
        }
        if(args[0].equalsIgnoreCase(command.getUsage(3))) {
            if(target == sender || !sender.areFriend(target)) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous ne pouvez pas retirer de vos amis avec ce joueur pour des raisons internes.");
                return;
            }
            sender.removeFriend(target);
            target.removeFriend(sender);
            sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous avez supprimé §b" + target.getBukkitPlayer().getDisplayName() + "§r §cde vos amis.");
            if(target.getBukkitPlayer().isOnline()) target.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + sender.getBukkitPlayer().getDisplayName() + "§r §cvous a supprimé de sa liste d'amis !");
            return;
        }
        if(args[0].equalsIgnoreCase(command.getUsage(4))) {
            if(!sender.containsFriendRequest(target)) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous n'avez pas reçu de demande d'ami de ce joueur.");
                return;
            }
            if(sender.getFriends().size() >= sender.getRank().getMaxFriends()) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous avez atteint votre limite d'amis.");
                return;
            }
            sender.removeFriendRequest(target);
            sender.addFriend(target);
            target.addFriend(sender);
            sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§aVous êtes maintenant ami avec §b" + target.getBukkitPlayer().getDisplayName() + "§r §a!");
            if(target.getBukkitPlayer().isOnline()) target.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + sender.getBukkitPlayer().getDisplayName() + "§r §aa accepté votre demande d'ami !");
        }
        if(args[0].equalsIgnoreCase(command.getUsage(5))) {
            if(!sender.containsFriendRequest(target)) {
                sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous n'avez pas reçu de demande d'ami de ce joueur.");
                return;
            }
            sender.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§cVous avez refusé la demande d'ami de §b" + target.getBukkitPlayer().getDisplayName() + "§r §a!");
            if(target.getBukkitPlayer().isOnline()) target.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + sender.getBukkitPlayer().getDisplayName() + "§r §ca refusé votre demande d'ami.");
        }
    }
}
