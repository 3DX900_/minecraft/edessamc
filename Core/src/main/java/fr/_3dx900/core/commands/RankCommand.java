package fr._3dx900.core.commands;

import fr.edessamc.api.commands.Command;
import fr.edessamc.api.commands.CommandHandler;
import fr.edessamc.api.commands.CommandImpl;
import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.rank.Rank;
import fr.edessamc.api.utils.text.Prefix;

public class RankCommand implements CommandImpl {

    @CommandHandler(command = "rank", description = "", usages = {"list"}, rank = Rank.RESPONSIBLE)
    public void process(String[] args, EPlayer sender, Command command) {
        if(args.length < 2 || Rank.getByName(args[1]) == Rank.PLAYER) {
            //TODO: SEND HELP
            return;
        }
        if(args[0].equalsIgnoreCase(command.getUsage(0))) {
            //TODO: RANK LIST
            return;
        }
        EPlayer target = EPlayer.getPlayer(args[0]);
        target.setRank(Rank.getByName(args[1]));
        sender.getBukkitPlayer().sendMessage(Prefix.RANK + "§e" + target.getBukkitPlayer().getDisplayName() + "§r §7a été gradé " + Rank.getByName(args[1]).getPrefix() + "§7.");
    }
}
