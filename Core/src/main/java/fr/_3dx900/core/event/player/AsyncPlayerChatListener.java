package fr._3dx900.core.event.player;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.misc.Level;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

    @EventHandler()
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        EPlayer player = EPlayer.getPlayer(event.getPlayer().getUniqueId());
        event.setFormat("§b" + Level.get(player.getExperience()) + "§r §f§l|§r " + player.getRank().getPrefix() + " " + player.getBukkitPlayer().getDisplayName() + "§r §8»§r " + player.getRank().getColor() + event.getMessage());
    }
}
