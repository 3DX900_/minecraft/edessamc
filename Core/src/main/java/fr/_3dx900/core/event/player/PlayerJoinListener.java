package fr._3dx900.core.event.player;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.utils.text.Prefix;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler()
    public void onPlayerJoin(PlayerJoinEvent event) {
        EPlayer player = EPlayer.getPlayer(event.getPlayer());

        player.getOnlineFriends().forEach(friend -> {friend.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + player.getBukkitPlayer().getName() + "§r §a§ovient de se connecter !");});
        player.getBukkitPlayer().sendMessage("§f➼ §7Ami en ligne : " + player.getOnlineFriends().toString().replace("[", "").replace("]", "").replaceAll(",", "§7,§b "));
    }
}
