package fr._3dx900.core.event.player;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.utils.text.Prefix;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler()
    public void onPlayerQuit(PlayerQuitEvent event) {
        EPlayer player = EPlayer.getPlayer(event.getPlayer());

        player.getOnlineFriends().forEach(friend -> {friend.getBukkitPlayer().sendMessage(Prefix.FRIEND + "§b" + player.getBukkitPlayer().getName() + "§r §c§os'est déconnecté.");});
    }
}
