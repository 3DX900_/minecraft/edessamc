package fr._3dx900.core.player.menu;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.misc.menu.Menu;
import fr.edessamc.api.utils.EItem;

public class MainMenu extends Menu {

    public MainMenu(EPlayer author) {
        super("Menu principal", EItem.MAIN_MENU, author);
    }

    @Override
    public void init() {
        this.setItem(28, EItem.MAIN_HUB, event -> this.close());
        this.setItem(34, EItem.HUB_SELECTOR, event -> this.getAuthor().openMenu(null));
    }

    @Override
    public void update() {
    }
}
