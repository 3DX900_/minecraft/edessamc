package fr.edessamc.hub;

import fr._3dx900.edessamc.server.ServerType;
import fr.edessamc.api.API;
import org.bukkit.plugin.java.JavaPlugin;

public class Hub extends JavaPlugin {

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    public void onEnable() {
        super.onEnable();

        API.getEServer().setType(ServerType.HUB);
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
