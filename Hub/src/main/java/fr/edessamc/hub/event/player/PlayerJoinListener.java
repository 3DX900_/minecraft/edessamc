package fr.edessamc.hub.event.player;

import fr.edessamc.api.player.EPlayer;
import fr.edessamc.api.player.rank.Rank;
import fr.edessamc.api.utils.EItem;
import fr.edessamc.api.utils.ELocation;
import fr.edessamc.hub.player.HubPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    @EventHandler()
    public void onPlayerJoin(PlayerJoinEvent event) {
        HubPlayer player = (HubPlayer) EPlayer.getPlayer(event.getPlayer());

        player.teleport(ELocation.PLAYER_HUB);

        player.fillInventory();
        player.setInventoryItem(0, EItem.MAIN_MENU);
        player.setInventoryItem(1);
        if(player.getRank().haveAccess(Rank.ADMINISTRATOR)) player.setInventoryItem(3);
        player.setInventoryItem(7);
        player.setInventoryItem(8);
    }
}
