package fr.edessamc.hub.player;

import fr.edessamc.api.player.EPlayer;

import java.util.UUID;

public class HubPlayer extends EPlayer {

    public HubPlayer(UUID uniqueId) {
        super(uniqueId);
    }
}
