package fr.edessamc.hub.player.menu;

import fr.edessamc.api.player.misc.menu.Menu;
import fr.edessamc.api.player.misc.menu.MenuHandler;
import fr.edessamc.api.utils.EItem;

public class MainMenu extends Menu {

    @MenuHandler(name = "Menu principal")
    public MainMenu() {
        super(EItem.MAIN_MENU);
    }
}
