package fr.edessamc.proxy;

import fr.edessamc.proxy.language.Lang;
import net.md_5.bungee.api.plugin.Plugin;

public class Proxy extends Plugin {

    private static Proxy instance;

    @Override
    public void onLoad() {
        Proxy.instance = this;

        Lang.ENGLISH.supportTranslate();
        Lang.FRENCH.supportTranslate();
        Lang.SPANISH.supportTranslate();
        Lang.DEUTSCH.supportTranslate();
    }

    @Override
    public void onEnable() {
    }

    @Override
    public void onDisable() {
    }

    public static Proxy getInstance() {
        return instance;
    }
}
