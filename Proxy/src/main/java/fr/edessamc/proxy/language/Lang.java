package fr.edessamc.proxy.language;

import fr.edessamc.proxy.Proxy;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.HashMap;

public enum Lang {

    ENGLISH("EN-en"), FRENCH("FR-fr"), SPANISH("ES-es"), DEUTSCH("DE-de");

    private String indicate;
    private HashMap<String, String> translations;

    Lang(String indicate) {
        this.indicate = indicate;
        this.translations = new HashMap<>();
    }

    public void supportTranslate() {
        if (!Proxy.getInstance().getDataFolder().exists())
            Proxy.getInstance().getDataFolder().mkdir();
        File file = new File(Proxy.getInstance().getDataFolder(), "lang."+ this.getIndicate() +".yml");
        if (!file.exists()) {
            try (InputStream in = Proxy.getInstance().getResourceAsStream("lang."+ this.toString() +".yml")) {
                Files.copy(in, file.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Configuration configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(Proxy.getInstance().getDataFolder(), "lang."+ this.getIndicate() +".yml"));
            configuration.getKeys().forEach(key -> this.putTranslation(key, configuration.getString(key)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIndicate() {
        return indicate;
    }

    public HashMap<String, String> getTranslations() {
        return translations;
    }

    public String putTranslation(String key, String value) {
        this.translations.put(key, value);
        return value;
    }
}
