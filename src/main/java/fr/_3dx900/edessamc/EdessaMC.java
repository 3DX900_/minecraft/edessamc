package fr._3dx900.edessamc;

import fr._3dx900.edessamc.server.EServer;
import fr._3dx900.edessamc.server.ServerType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EdessaMC {

    private static List<EServer> servers = new ArrayList<>();

    public static void main(String[] args) {}

    public static List<EServer> getServers() {
        return EdessaMC.servers;
    }

    public static EServer getServer(UUID uniqueId) {
        for(EServer server : EdessaMC.getServers()) {
            if(server.getUniqueId() == uniqueId)
                return server;
        }
        return new EServer(uniqueId);
    }

    public static List<EServer> getServers(ServerType type) {
        List<EServer> servers = new ArrayList<>();
        for(EServer server : EdessaMC.getServers()) {
            if(server.getType() == type)
                servers.add(server);
        }
        return servers;
    }
}
