package fr._3dx900.edessamc.server;

import java.util.UUID;

public class EServer {

    private UUID uniqueId;
    private String name;
    private ServerType type;
    private ServerState state;

    private Integer port;

    public EServer(UUID uniqueId) {
        this(uniqueId, ServerType.UNKNOWN);
    }

    public EServer(ServerType type) {
        this(UUID.randomUUID(), type);
    }

    public EServer(UUID uniqueId, ServerType type) {
        this.uniqueId = uniqueId;
        this.type = type;
        this.state = ServerState.WAITING;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public EServer setName(String name) {
        this.name = name;
        return this;
    }

    public ServerType getType() {
        return type;
    }

    public EServer setType(ServerType type) {
        this.type = type;
        return this;
    }

    public ServerState getState() {
        return state;
    }

    public EServer setState(ServerState state) {
        this.state = state;
        return this;
    }

    public Integer getPort() {
        return port;
    }

    public EServer setPort(Integer port) {
        this.port = port;
        return this;
    }
}
