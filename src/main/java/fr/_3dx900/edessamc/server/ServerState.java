package fr._3dx900.edessamc.server;

public enum ServerState {
    WAITING, LOADING, ACCESSIBLE, UNACCESSIBLE, CLOSED;
}
