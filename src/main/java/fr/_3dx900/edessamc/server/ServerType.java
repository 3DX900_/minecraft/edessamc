package fr._3dx900.edessamc.server;

public enum ServerType {
    PROXY, HUB, GAME, UNKNOWN;
}
